<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page session="true"%>
<html>
<head>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css" />" />
<link rel="stylesheet" href="<c:url value="/resources/css/codemirror.css" />" />
<link rel="stylesheet" href="<c:url value="/resources/css/eclipse.css" />" />
<link rel="stylesheet" href="<c:url value="/resources/css/admin.css" />" />
</head>
<body>
	<script src="<c:url value="/resources/js/admin/messageHandler.js" />"></script>
	<%@ include file="../adds/header.jsp"%>

	<div class="container">




		<h1>Edit your css and html for life chat</h1>
		<div ng-controller="myCtrl">

			<div data-hbo-tabs id="tabs">
				<ul>
					<li><a href="#lifechat">Life Chats</a></li>
					<li><a id="managementTab" href="#management">Management</a></li>
				</ul>
				<div id="lifechat">
					<p><%@ include file="../adds/conversationManagement.jsp"%></p>
				</div>
				<div id="management">
					<p><%@ include file="../adds/formChat.jsp"%></p>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="../adds/footer.jsp"%>
	<%@ include file="../adds/dialog.jsp"%>


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="<c:url value="/resources/js/codemirror.js" />"></script>
	<script src="<c:url value="/resources/js/xml.js" />"></script>
	<script src="<c:url value="/resources/js/css.js" />"></script>
	<script src="<c:url value="/resources/js/admin/utils.js" />"></script>
	<script src="<c:url value="/resources/js/admin/admin.js" />"></script>
	<script src="<c:url value="/resources/js/admin/messageTransactions.js" />"></script>

	<c:if test="${not empty message}">
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>${message}</strong>
		</div>
		<c:if test="${not empty situation}">
			<script>
				$(document).ready(
								function() {
									chatCoreForm.moveHere();
									lifeChat.init('<sec:authentication property="principal.username" />');
									lifeChat.checkMessages();
								});
			</script>
		</c:if>

	</c:if>

	<script>
		$(document).ready(
						function() {
							lifeChat.init('<sec:authentication property="principal.username" />');
							lifeChat.checkMessages();
						});
	</script>
</body>
</html>
<div id="header" class="container" >
	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<div class="col-sm-12 clearfix" style="background-color:lavender;">
			<h2 class="pull-left" >Life chat Control Panel</h2>
			<h2 class="pull-right" >
				Welcome : ${pageContext.request.userPrincipal.name} | <a
					href="<c:url value="/admin/settings"/>"> Settings</a> | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</div>
	</c:if>
</div>
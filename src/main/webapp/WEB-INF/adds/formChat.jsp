<form class="form-inline" modelAttribute="chatCoreDto" id="chatCoreform" action="<c:url value="/saveChatCore" />" method="post" role="form">
        <div class="form-group form-html">
          	<label class="sr-only" for="email">Html:</label>
       	  	<textarea id="htmlChat" name="html" class="form-control" rows="10" cols="50">${chatCore.html}</textarea>
        </div>
        <div class="form-group form-css">
     	  	<label class="sr-only" for="pwd">CSS:</label>
        	<textarea id="cssChat" name="css" class="form-control" rows="10" cols="50">${chatCore.css}</textarea>
        </div>
      	<div class="row">
      		<div class="col-sm-12 clearfix pull-left">
	      		<button type="submit" class="btn btn-default">Save</button>
	        	<button type="button" id="preview" class="btn btn-default">Preview</button>
        	</div>
      	</div>
        <input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
</form>
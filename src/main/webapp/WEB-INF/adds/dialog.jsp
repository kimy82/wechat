<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
	            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="messageslist" >
                </div>
                <input type="text" class="input-message" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closemodal"  data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary envia-message">Envia</button>
        </div>
    </div>
  </div>
</div>
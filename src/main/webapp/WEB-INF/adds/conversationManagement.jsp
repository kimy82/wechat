<div class="row" id="lifeconverses" style="width: 90%">
</div>
	<c:forEach var="chat" items="${chats}">
		<div class="row" id="${chat.userFrom}" style="width: 70%">
			<div class="col-md-3">${chat.userFrom}</div>
			<div class="col-md-5">
				<c:forEach var="message"  items="${chat.messages}" varStatus="loop">
					 <c:if test="${loop.first}">
					 	<c:set value="15" var="num"/>
					 	<c:choose>
						 	<c:when test="${fn:length(message.message)>num}">
						 		${fn:substring(message.message, 0, num)} ...   
						 	</c:when>
						 	<c:otherwise>
						 		${message.message}
						 	</c:otherwise>
						</c:choose>
					 	<fmt:formatDate pattern="yyyy-MM-dd" value="${message.date}" />
					 </c:if>
					
					<script>
						messageHandler.createMessage('${message.message}','${message.date}','${chat.userFrom}','${message.username}');
					</script>
				</c:forEach>
			</div>
			<div class="col-md-2">
				<a class="deleteChat btn btn-default" data_username="${chat.userFrom}" id="btn_${chat.userFrom}_close" href="#">Delete</a>
			</div>
			<div class="col-md-2">
				<a class="openChat btn btn-default" data_username="${chat.userFrom}" id="btn_${chat.userFrom}_open" href="#">Open</a>
			</div>
		</div>
	</c:forEach>


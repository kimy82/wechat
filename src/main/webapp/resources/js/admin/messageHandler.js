function Message(){};

/**
 * Date of message
 */
Message.prototype.data;
Message.prototype.getData = function(){
	return this.data;
};

Message.prototype.data;
Message.prototype.getData = function(){
	return this.data;
};
Message.prototype.setData = function(data){
	this.data = data;
};

/**
 * Message
 */
Message.prototype.message;
Message.prototype.getMessage = function(){
	return this.message;
};
Message.prototype.setMessage = function(message){
	this.message = message;
};

/**
 * UserOf message
 */
Message.prototype.userOf;
Message.prototype.getUserOf = function(){
	return this.userOf;
};
Message.prototype.setUserOf = function(userOf){
	this.userOf = userOf;
};


var messageHandler = {
	_init : function(){
		messageHandler.messagesStore = [];
	},
	getMessagesFromUser : function(userFrom){
		if(typeof userFrom === 'undefined'){
			throw 'unable to recover messages from undefined user';
		}
		var messagesOf = messageHandler.messagesStore[userFrom];
		return messagesOf;
		
	},
	setMessage : function(message , userFrom, userOf){
		var messagesOf = messageHandler.messagesStore[userFrom];
		if(typeof messagesOf === 'undefined'){
			messageHandler.messagesStore[userFrom] = [message];
		}else{
			messagesOf.push(message);
		}
	},
	createMessage : function(messageString,data,userFrom, userOf){
		var message = new Message();
		message.setMessage(messageString);
		message.setData(data);
		message.setUserOf(userOf);
		messageHandler.setMessage(message,userFrom);
		return message;
	}
};

messageHandler._init();
//initialise tabs
$(function() {
    $( "#tabs" ).tabs();
});


/**
 * Code mirror per html
 */
var htmlChat = CodeMirror.fromTextArea(document.getElementById("htmlChat"), {
    lineNumbers: true,
    mode: "text/html"
  });

/**
 * Code mirror per css
 */
var cssChat = CodeMirror.fromTextArea(document.getElementById("cssChat"), {
    lineNumbers: true,
    mode: "text/css"
  });

/**
 * Preview dialog controller
 */
var previewController = {
		/**
		 * Adds event in preview button
		 */
		init : function(){$("#preview").on('click', previewController.render);},
		/**
		 * Adds css and html to actual page
		 */
		render : function(){
			var html = htmlChat.getValue();		
			var css = cssChat.getValue();
			$('head').append('<style>'+ css +'</style>');
			$( "body" ).append(html);
		}
};
previewController.init();

/**
 * Chat form per html and css controller
 */
var chatCoreForm = {
		/**
		 * Add event to form submit.
		 */
		init : function(){
			$("#chatCoreForm").on("click",chatCoreForm.populate);
		},
		/**
		 * Populate new html and css from codemirror to textarea
		 */
		populate : function(){
			$("#htmlChat").html(htmlChat.getValue());
			$("#cssChat").html(cssChat.getValue());
		},
		/**
		 * Selects form tab
		 */
		moveHere : function(){
			$("#managementTab").click();
		}
}

/**
 * Controlls de deletion and opening of the chats.
 */
var conversationController = {
		/**
		 * Adds event on open and delete chat buttons
		 */
		init : function(){
			$(".deleteChat").on("click",conversationController.deleteChat);
			$(".openChat").on("click",conversationController.openChat);
		},
		/**
		 * delete the chat.
		 */
		deleteChat : function(){
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			var userFrom = $(this).attr("data_username");
		
			$.ajax({
				  type: "DELETE",
				  url: "/deleteChat/"+userFrom,
				  data: {},
				  beforeSend: function(xhr){xhr.setRequestHeader(header, token);},
				  success: conversationController._removeChatInPage(userFrom)
			});
		},
		/**
		 * Remove the row of the chat.
		 */
		_removeChatInPage : function(userFrom) {
			$("#"+userFrom).remove();
		},
		/**
		 * Open the chat in a conversation dialog.
		 */
		openChat : function(){
			var userFrom = $(this).attr("data_username");
			if(typeof $("#"+userFrom+"_modal")[0] !== 'undefined'){
				return;
			}
			var messages = messageHandler.getMessagesFromUser(userFrom);
			
			var modal = $("#basicModal").clone();
			$(modal).attr('id',userFrom+"_modal");
			
			conversationController.setMessages(modal,userFrom);
			
			$(modal).draggable({
			    handle: ".modal-header"
			});
			
			$(modal).on('dragstart',modalBehaivor.onDragStart);
			$(modal).on('drag',modalBehaivor.onDrag);
			$(modal).on('dragstop',modalBehaivor.onDragStop);
			
			$(modal).modal('show');
			$(modal).find(".modal-backdrop").removeClass("modal-backdrop");
			$(modal).css('display', 'inline-block');
			$(modal).css('position', 'fixed');
			$(modal).css('width', '300px');
			$(modal).css('height', '350px');
			$(modal).css('overflowY', 'hidden');
			$(modal).find('.modal-content').css('width', '300px');
			$(modal).find('.modal-content').css('height', '300px');
			$(modal).find('.closemodal').attr('data_userFrom', userFrom);
			$(modal).find('.closemodal').on("click",conversationController.closeChatDialog);
			$(modal).find('.envia-message').on("click",lifeChat.enviaMessage);
			$(modal).find('.envia-message').attr('data_userFrom', userFrom);
			positionalManager.setNewDiv();
			$(modal).attr("ck_inline",'true');
			$(modal).attr("ck_inline_pos",positionalManager.numDivs);
			var marginRight = $(modal).attr("ck_inline_pos")*$(modal).width()-$(modal).width();
			$(modal).css('left','auto');
			$(modal).css('top','auto');
			$(modal).css('right',marginRight);
			$(modal).css('bottom','0');
			setTimeout(function(){utils.scrollBottom($(modal).find(".messagesList")[0]);},500);
			
		},
		setMessages : function(modal,userFrom){
			var messagesDiv = $(modal).find(".messagesList")[0];
			var messages = messageHandler.getMessagesFromUser(userFrom);
			for(i=0; i<messages.length; i++){
				var message = messages[i];
				var div =  document.createElement("div");
				if(lifeChat.username == message.getUserOf()){
					$(div).addClass(utils.getOwnerStyle());
				}else{
					$(div).addClass(utils.getNoOwnerStyle());
				}
				$(div).html(message.getMessage());
				$(messagesDiv).append(div);
			}
			$(modal).find('#myModalLabel').html(userFrom);
			
		},
		addNewMessage: function(userFrom,message,style){
			try{
				var messagesDiv = $('#'+userFrom+"_modal").find(".messageslist")[0];
				var div =  document.createElement("div");
				$(div).addClass(style);
				$(div).html(message);
				$(messagesDiv).append(div);
				utils.scrollBottom(messagesDiv);
			}catch(error){}
		},
		closeChatDialog : function(){
			positionalManager.removeDiv();
			var modalUserFrom = $(this).attr('data_userFrom');
			modalBehaivor.recenterChats($("#"+modalUserFrom+"_modal"));
		}
}
conversationController.init();

var modalBehaivor = {
		onDragStart : function (event){
			$(".modal").css( "zIndex", 1 );
            $( this ).css( "zIndex", 99999 );
		},
		onDrag : function(event){
			var thismodal = this;
			if($(thismodal).attr("ck_inline") == 'true'){
				//esta verticalment en posicio de swap
				var posOfModal = $(thismodal).attr("ck_inline_pos");
				if((event.clientY + $( thismodal ).height()) < $( window ).height()){
					//si no es l'ultim
					if(posOfModal < positionalManager.numDivs){
						modalBehaivor.recenterChats(thismodal);
					}
				}
				var x = positionalManager.getActualPosition(thismodal)- ($(thismodal).width()/2);
				var clientX = positionalManager.getCenterXInPage(thismodal);
				if(Math.abs($(window).width()-clientX - x)  > 200){
					if($(window).width()-clientX - x < 0){
						modalBehaivor.moveModal(parseInt(posOfModal)-1,'left');
						modalBehaivor.setPosition(thismodal,-1);
						return;
					}
					if($(window).width()-clientX - x > 0){
						modalBehaivor.moveModal(parseInt(posOfModal)+1,'right');
						modalBehaivor.setPosition(thismodal,+1);
						return;
					}
				}
			}else{
				if((event.clientY + $( this ).height()) > $( window ).height()){
					modalBehaivor.addChatDialogInline(this);
				}
			}
			
		},
		onDragStop : function(event){
		
			if((event.clientY + $( this ).height()) > $( window ).height()){
				//posiciona
				var rigth = positionalManager.getRight(this);
				
				modalBehaivor.addChatDialogInline(this);
				
				$(this).css("left",'auto');
				$(this).css("top",'auto');
				$(this).css("right",rigth+'px');
				$(this).css("bottom",'0px');
				return;
			}
			
		},
		/**
		 * Given a modal in inline zone which is leaving. Recenter other modals and remova it from inline zone.
		 */
		recenterChats : function(modal){
			var posOfModal = $(modal).attr("ck_inline_pos");
			if(!isNaN(posOfModal)){
				$( ".modal" ).each(function( index ) {
					
					 if($(this).attr("ck_inline") == 'true' && $(this).attr("ck_inline_pos") != posOfModal 
							 && $(this).attr("ck_inline_pos")>$(modal).attr("ck_inline_pos")){
						 //move to right	
						 var posicion = $(this).attr("ck_inline_pos");
						 $(this).attr("ck_inline_pos", parseInt(posicion)-1);
						 var newright = (parseInt($(this).attr("ck_inline_pos"))-1)*$(this).width();
						 $(this).css("right",newright+'px');
					 }
				});
			}else{
				throw "the dialog chat does not have a posicion";
			}
			
			modalBehaivor.removeChatDialogInline(modal);
			
		},
		/**
		 * Move modal in position posModal to the direction 'left' or 'right'.
		 */
		moveModal : function(posModal,direction){
			var modal = positionalManager.getModal(posModal);
			if(typeof modal === 'undefined' ){
				return;
			}
			if(direction == 'left'){
				modalBehaivor.setPosition(modal,+1);
			}
			if(direction == 'right'){
				modalBehaivor.setPosition(modal,-1);
			}
			var newright = (parseInt($(modal).attr("ck_inline_pos"))-1)*$(modal).width();
			$(modal).css("right",newright+'px');
		},
		/**
		 * Update ck_inlin_pos with a relative offset.
		 */
		setPosition : function(modal, relativePos){
			 var posicion = $(modal).attr("ck_inline_pos");
			 $(modal).attr("ck_inline_pos", parseInt(posicion)+relativePos);
		},
		/**
		 * Adds the modal in the inline zone.
		 */
		addChatDialogInline : function(modal){
			if( $(modal).attr("ck_inline") == 'false'){
				positionalManager.setNewDiv();
				$(modal).attr("ck_inline",'true');
				$(modal).attr("ck_inline_pos",positionalManager.numDivs);
			}
		},
		/**
		 * Remove modal from inline zone.
		 */
		removeChatDialogInline : function(modal){
			$(modal).attr("ck_inline_pos","");
			$(modal).attr("ck_inline","false");
			positionalManager.removeDiv();
		}
}


var positionalManager = {
		_init : function(){
			positionalManager.numDivs = 0;
		},
		setNewDiv : function(){
			positionalManager.numDivs++;
		},
		/**
		 * Returns rigth position where the dialog should be placed 
		 * or where is placed if it is allready in the inline zone.
		 */
		getRight : function(modal){
			var numOfPosicions = positionalManager.numDivs;
			var width = $(modal).width();
			if($(modal).attr("ck_inline") == 'true'){
				numOfPosicions = parseInt($(modal).attr("ck_inline_pos"))-1;
			}
			return width*numOfPosicions;
		},
		/**
		 * Returns X left position of the modal
		 */
		getActualPosition : function(modal){
			var width = $(modal).width();
			var posOfModal = $(modal).attr("ck_inline_pos");
			return width*parseInt(posOfModal);
		},
		removeDiv : function(){
			positionalManager.numDivs--;
		},
		/**
		 * Returns the modal in the position.
		 */
		getModal : function(pos){
			var modal;
			$( ".modal" ).each(function( index ) {
				 if($(this).attr("ck_inline") == 'true' && parseInt($(this).attr("ck_inline_pos")) == pos){
					modal = this;
				 }
			});
			return modal;
		},
		/**
		 * Returns the X in the center of the modal.
		 */
		getCenterXInPage : function(modal){
			var left = $(modal).offset().left;
			var centerX = left + $(modal).width()/2;
			return centerX;
		}
		
};
positionalManager._init();


/**
 * Life chat
 */
var lifeChat = {
		_setUsername : function(username){
			lifeChat.username = username;
		},
		init : function(username){
			lifeChat._setUsername(username);
			$(".envia-message").on('click',lifeChat.enviaMessage)
		},
		enviaMessage : function(){
			lifeChat._checkUserIsThere();
			var userTo = $(this).attr('data_userFrom'); 
			var message = $("#"+userTo+"_modal").find('.input-message').val();
			$("#"+userTo+"_modal").find('.input-message').val('');
			var style = utils.getOwnerStyle();
			
			conversationController.addNewMessage(userTo,message,style);
			
			
			var data={
					'userTo' : userTo,
					'userFrom' : lifeChat.username,
					'message':  message,
			}
			$.ajax({
				  type: "POST",
				  url: "/sentMessage",
				  data: data,
				  success: lifeChat.populateSentMessage,
			});
		},
		populateSentMessage : function(data){
			var message = messageHandler.createMessage(data.message,data.date,lifeChat.username, data.username);
		},
		checkMessages : function(){
			
			lifeChat._checkUserIsThere();
			$.ajax({
				  type: "GET",
				  url: "/checkMessages/"+lifeChat.username,
				}).done(lifeChat.populateReceivedMessage)
				.fail(function(e){console.log(e);});
		},
		populateReceivedMessage : function(data){
			console.log(data);
			if(data.messages != null && data.messages.length > 0){
				for(i=0;i<data.messages.length;i++){
					var message = messageHandler.createMessage(data.messages[i].message,data.messages[i].date, data.messages[i].username);
					conversationController.addNewMessage(data.messages[i].username,message);
				}
			}
			lifeChat.checkMessages();
		},
		_checkUserIsThere : function(){
			if(typeof lifeChat.username === 'undefined'){
				throw 'user is undefined';
			}
		}
}
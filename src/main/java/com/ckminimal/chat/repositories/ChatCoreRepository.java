package com.ckminimal.chat.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ckminimal.chat.entities.ChatCore;

public interface ChatCoreRepository extends CrudRepository<ChatCore, Long> {

	final String FIND_BY_USER = "select cc from ChatCore as cc where cc.user.username= :username";

	@Query(FIND_BY_USER)
	ChatCore findByUserId(@Param("username") String username);

}

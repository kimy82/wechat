package com.ckminimal.chat.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ckminimal.chat.entities.ChatDialog;

public interface ChatDialogRepository extends CrudRepository<ChatDialog, Long> {

	final String QUERY_BY_USER = "select cd from ChatDialog as cd where cd.user.username=:username";

	@Query(QUERY_BY_USER)
	ChatDialog findByUserName(@Param("username") String username);

}

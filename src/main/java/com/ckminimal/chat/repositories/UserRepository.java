package com.ckminimal.chat.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ckminimal.chat.entities.User;

public interface UserRepository extends CrudRepository<User, String> {

}

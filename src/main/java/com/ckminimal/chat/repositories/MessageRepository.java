package com.ckminimal.chat.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.ckminimal.chat.entities.Message;

public interface MessageRepository extends CrudRepository<Message, Long> {

	final String DELETE_BY_USER = "delete from Message as ms where ms.id=:id";

	@Modifying
	@Transactional
	@Query(DELETE_BY_USER)
	void deleteByUserName(@Param("id") Long id);
}

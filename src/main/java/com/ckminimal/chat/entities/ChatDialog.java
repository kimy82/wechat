package com.ckminimal.chat.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="chatdialog")
public class ChatDialog {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CHATDIALOG_ID", unique = true, nullable = false)
	private Long id;

	@OneToOne
	private User user;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "chatDialog")
	private List<Message> messages;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
}

package com.ckminimal.chat.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity(name = "users")
public class User {

	@Id
	private String		username;

	@Column(nullable = false)
	private String		email;

	@Column(nullable = false)
	private String		password;

	@Column(nullable = false)
	private int			enabled;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private ChatDialog	chatDialog;

	@OneToOne
	private ChatCore	chatcore;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ChatDialog getChatDialog() {
		return chatDialog;
	}

	public void setChatDialog(ChatDialog chatDialog) {
		this.chatDialog = chatDialog;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public ChatCore getChatcore() {
		return chatcore;
	}

	public void setChatcore(ChatCore chatcore) {
		this.chatcore = chatcore;
	}

}

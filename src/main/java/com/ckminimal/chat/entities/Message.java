package com.ckminimal.chat.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "message")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long		id;

	@Column
	private String		username;

	@Column
	private String		message;

	@Column
	private Date		date;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHATDIALOG_ID", nullable = false)
	private ChatDialog	chatDialog;

	public ChatDialog getChatDialog() {
		return chatDialog;
	}

	public void setChatDialog(ChatDialog chatDialog) {
		this.chatDialog = chatDialog;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}

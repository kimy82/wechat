package com.ckminimal.chat.dto;

import java.util.List;

public class ChatDialogDTO {
	
	private String userFrom;
	
	private List<MessageDTO> messages;

	public String getUserFrom() {
		return userFrom;
	}

	public void setUserFrom(String userFrom) {
		this.userFrom = userFrom;
	}

	public List<MessageDTO> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageDTO> messages) {
		this.messages = messages;
	}
	
	
}

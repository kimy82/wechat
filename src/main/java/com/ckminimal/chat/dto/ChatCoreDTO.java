package com.ckminimal.chat.dto;

/**
 * DTO per el form del Chatcore.
 * 
 * @author kim
 *
 */
public class ChatCoreDTO {

	private String html;
	private String css;
	
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getCss() {
		return css;
	}
	public void setCss(String css) {
		this.css = css;
	}
}

package com.ckminimal.chat.config;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth)
			throws Exception {

		auth.jdbcAuthentication()
				.dataSource(dataSource)
				.usersByUsernameQuery(
						"select username,password, enabled from users as u where u.username=?")
				.authoritiesByUsernameQuery(
						"select user_username,authority from user_roles as auth where auth.user_username=?");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
				.antMatchers("/admin/**")
				.access("hasRole('ROLE_ADMIN')")
				.antMatchers("/admin/update**")
				.access("hasRole('ROLE_ADMIN')")
				.and()
				.formLogin()
				.successHandler(savedRequestAwareAuthenticationSuccessHandler())
				.loginPage("/login").failureUrl("/login?error")
				.loginProcessingUrl("/auth/login_check")
				.usernameParameter("username").passwordParameter("password")
				.and().logout().logoutUrl("/j_spring_security_logout").logoutSuccessUrl("/login?logout").and().csrf().requireCsrfProtectionMatcher(new RequestMatcher() {
					private Pattern		allowedMethods	= Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
					private RegexRequestMatcher	apiMatcher		= new RegexRequestMatcher("/sentMessage", null);

					public boolean matches(HttpServletRequest request) {
						// No CSRF due to allowedMethod
						if (allowedMethods.matcher(request.getMethod()).matches())
							return false;

						// No CSRF due to api call
						if (apiMatcher.matches(request))
							return false;

						// CSRF for everything else that is not an API call or
						// an allowedMethod
						return true;
					}

				})
				.and().rememberMe()
				.tokenRepository(persistentTokenRepository())
				.tokenValiditySeconds(1209600);

	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
		db.setDataSource(dataSource);
		return db;
	}

	@Bean
	public SavedRequestAwareAuthenticationSuccessHandler savedRequestAwareAuthenticationSuccessHandler() {
		SavedRequestAwareAuthenticationSuccessHandler auth = new SavedRequestAwareAuthenticationSuccessHandler();
		auth.setTargetUrlParameter("targetUrl");
		return auth;
	}

}
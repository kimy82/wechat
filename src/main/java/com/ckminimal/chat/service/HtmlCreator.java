package com.ckminimal.chat.service;

import com.ckminimal.chat.entities.ChatCore;

/**
 * 
 * Provides html for chat.
 * 
 * @author ckminimal
 *
 */
public interface HtmlCreator {

	ChatCore getBaseHtml(String username);

}

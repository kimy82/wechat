package com.ckminimal.chat.service;

import com.ckminimal.chat.dto.ChatDialogDTO;
import com.ckminimal.chat.dto.MessageDTO;

public interface NotificationCache {

	/**
	 * Adds notification message from user to another user
	 * 
	 * @param userTo
	 * @param userFrom
	 * @param message
	 */
	void addNotification(String userTo, String userFrom, MessageDTO message);

	/**
	 * Gets all notifications forUser
	 * 
	 * @param forUser
	 * @return {@link ChatDialogDTO} with list of {@link MessageDTO} or null if
	 *         nothing is waiting.
	 */
	ChatDialogDTO getNotifications(String forUser);

	/**
	 * Delete notifications forUser
	 * 
	 * @param forUser
	 */
	void deleteNotifications(String forUser);

}

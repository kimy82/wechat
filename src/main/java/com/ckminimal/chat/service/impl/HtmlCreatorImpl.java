package com.ckminimal.chat.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ckminimal.chat.entities.ChatCore;
import com.ckminimal.chat.entities.User;
import com.ckminimal.chat.repositories.ChatCoreRepository;
import com.ckminimal.chat.repositories.UserRepository;
import com.ckminimal.chat.service.HtmlCreator;

/**
 * 
 * Implementation for {@link HtmlCreator}.
 * 
 * @author ckminimal
 *
 */
@Service
public class HtmlCreatorImpl implements HtmlCreator {

	private final String		BASE_HTML	= "<div class='chatconatiner'>\n"
			+ "<div class='messagecontainer'>\n"
			+ "<div class='messageslist' ></div>"
			+ "<input type='text' id='userFrom' /><br>\n"
			+ "<input type='text' id='message'/><br>\n"
			+ "<input type='button' id='enviaMessage' value='enviaMessage' /><br>\n"
			+ "</div>\n"
			+ "<div class='writercontainer'>\n</div>\n"
			+ "</div>";

	private final String		BASE_CSS	= ".chatconatiner{\n"
			+ "	position: fixed;\n"
			+ "	bottom: 0;\n"
			+ "	right:15px;\n"
			+ "	background: #e4e4e4;\n"
			+ "}"
			+ ".messageslist{\n"
			+ "   width: 100%;\n"
			+ "   height: 150px;\n"
			+ "	overflow-y: scroll;\n"
			+ "}";

	@Resource
	private ChatCoreRepository	chatCoreRepository;

	@Resource
	private UserRepository		userRepository;

	public ChatCore getBaseHtml(String username) {

		ChatCore chatCore = this.chatCoreRepository.findByUserId(username);
		if (chatCore == null) {
			chatCore = new ChatCore();
			chatCore.setHtml(BASE_HTML);
			chatCore.setCss(BASE_CSS);
			User user = userRepository.findOne(username);
			chatCore.setUser(user);
			this.chatCoreRepository.save(chatCore);
		}
		return chatCore;
	}

}

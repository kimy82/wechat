package com.ckminimal.chat.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.ckminimal.chat.dto.ChatDialogDTO;
import com.ckminimal.chat.dto.MessageDTO;
import com.ckminimal.chat.service.NotificationCache;

/**
 * 
 * Implementation for storing notifications for the user in lifechat.
 * 
 * @author kim
 *
 */
@Service
public class NotificationCacheImpl implements NotificationCache {

	private final Map<String, ChatDialogDTO> users = new HashMap<String, ChatDialogDTO>();

	public void addNotification(String userTo, String userFrom, MessageDTO message) {
		ChatDialogDTO notificacions = new ChatDialogDTO();

		if (!users.containsKey(userTo)) {
			List<MessageDTO> messages = new ArrayList<MessageDTO>();
			messages.add(message);

			notificacions.setMessages(messages);
			users.put(userTo, notificacions);
		} else {
			notificacions = users.get(userTo);
			notificacions.getMessages().add(message);
			users.put(userTo, notificacions);
		}
	}

	/**
	 * Checks for messages stored in {@link #users}.
	 * 
	 * @return {@link ChatDialogDTO} or null if no messages are stored.
	 * 
	 */
	public ChatDialogDTO getNotifications(String forUser) {
		ChatDialogDTO notificacions = null;
		if (users.containsKey(forUser)) {
			notificacions = users.get(forUser);
			notificacions.setUserFrom(forUser);
		}
		return notificacions;
	}

	public void deleteNotifications(String forUser) {
		if (users.containsKey(forUser)) {
			users.remove(forUser);
		}
	}
}

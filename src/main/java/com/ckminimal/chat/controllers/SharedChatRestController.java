package com.ckminimal.chat.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.ckminimal.chat.dto.ChatDialogDTO;
import com.ckminimal.chat.dto.MessageDTO;
import com.ckminimal.chat.entities.ChatDialog;
import com.ckminimal.chat.entities.Message;
import com.ckminimal.chat.entities.User;
import com.ckminimal.chat.repositories.ChatDialogRepository;
import com.ckminimal.chat.repositories.MessageRepository;
import com.ckminimal.chat.repositories.UserRepository;
import com.ckminimal.chat.service.NotificationCache;

/**
 * Shared rest controller for admin and user.
 * 
 * @author kim
 *
 */
@EnableScheduling
@RestController
public class SharedChatRestController extends HelperController {

	/**
	 * Map of {@link DeferredResult} waiting for notification.
	 */
	private final Map<String, DeferredResult<ChatDialogDTO>>	deferredResults	= new HashMap<String, DeferredResult<ChatDialogDTO>>();

	@Resource
	private ChatDialogRepository								chatDialogRepository;

	@Resource
	private MessageRepository									messageRepository;

	@Resource
	private NotificationCache									notificationCache;

	@Resource
	private UserRepository										userRepository;

	/**
	 * Check for new messages.
	 * 
	 * @param userTo
	 * @return {@link ChatDialogDTO}
	 */
	@RequestMapping(value = "/checkMessages/{userTo}", method = RequestMethod.GET)
	public DeferredResult<ChatDialogDTO> checkMessages(
			@PathVariable(value = "userTo") final String userTo) {

		ChatDialogDTO chatDialog = new ChatDialogDTO();
		DeferredResult<ChatDialogDTO> deferredResult = new DeferredResult<ChatDialogDTO>(
				60000L, chatDialog);
		ChatDialogDTO chatDialogRetrieved = notificationCache
				.getNotifications(userTo);
		if (chatDialogRetrieved != null) {
			deferredResult.setResult(chatDialogRetrieved);
		}
		deferredResult.onCompletion(new Runnable() {

			public void run() {
				notificationCache.deleteNotifications(userTo);
				removeDeferredResult(userTo);
			}
		});

		addDeferredResult(deferredResult, userTo);

		return deferredResult;
	}

	/**
	 * Sends a message to userTo coming from userFrom.
	 * 
	 * @param userTo
	 * @param userFrom
	 * @param message
	 */
	@RequestMapping(value = "/sentMessage", method = RequestMethod.POST)
	public MessageDTO sendMessage(@RequestParam(value = "userTo") String userTo,
			@RequestParam(value = "userFrom") String userFrom,
			@RequestParam(value = "message") String message) {

		Message messageObj = addNewMessage(userFrom, userTo, message);
		MessageDTO messageDto = new MessageDTO();
		BeanUtils.copyProperties(messageObj, messageDto);
		notificationCache.addNotification(userTo, userFrom, messageDto);
		return messageDto;
	}

	/**
	 * Adds the deferred result to the {@link #deferredResults}
	 * 
	 * @param deferredResult
	 * @param forUser
	 */
	public void addDeferredResult(DeferredResult<ChatDialogDTO> deferredResult,
			String forUser) {
		deferredResults.put(forUser, deferredResult);

	}

	/**
	 * Remove deferredresult for key forUser.
	 * 
	 * @param forUser
	 */
	public void removeDeferredResult(String forUser) {
		if (deferredResults.containsKey(forUser)) {
			deferredResults.remove(forUser);
		}
	}

	/**
	 * Checks if there are messages waiting for some key.
	 */
	@Scheduled(fixedDelay = 4000)
	public void processResults() {
		for (Entry<String, DeferredResult<ChatDialogDTO>> entry : deferredResults.entrySet()) {
			ChatDialogDTO chatDialogDTO = this.notificationCache.getNotifications(entry.getKey());
			if (chatDialogDTO != null) {
				entry.getValue().setResult(chatDialogDTO);
			}
		}
	}

	/**
	 * Create a new message in DB.
	 * 
	 * @param userFrom
	 * @param userTo
	 * @param message
	 */
	private Message addNewMessage(String userFrom, String userTo, String message) {
		ChatDialog chatDialog = this.chatDialogRepository
				.findByUserName(userTo);

		if (chatDialog == null) {
			chatDialog = new ChatDialog();
			User user = this.userRepository.findOne(userTo);
			chatDialog.setUser(user);
			chatDialog.setMessages(new ArrayList<Message>());
			chatDialog = this.chatDialogRepository.save(chatDialog);
		}
		Message messageObj = new Message();
		messageObj.setDate(new Date());
		messageObj.setMessage(message);
		messageObj.setUsername(userFrom);
		messageObj.setChatDialog(chatDialog);
		this.messageRepository.save(messageObj);
		return messageObj;
	}

}

package com.ckminimal.chat.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ckminimal.chat.dto.ChatCoreDTO;
import com.ckminimal.chat.dto.ChatDialogDTO;
import com.ckminimal.chat.dto.MessageDTO;
import com.ckminimal.chat.entities.ChatCore;
import com.ckminimal.chat.entities.ChatDialog;
import com.ckminimal.chat.entities.Message;
import com.ckminimal.chat.entities.User;
import com.ckminimal.chat.repositories.ChatCoreRepository;
import com.ckminimal.chat.repositories.ChatDialogRepository;
import com.ckminimal.chat.repositories.UserRepository;
import com.ckminimal.chat.service.HtmlCreator;

/**
 * Controller actions within admin panel.
 * 
 * @author kim
 *
 */
@Controller
public class AdminChatController extends HelperController {

	@Resource
	private HtmlCreator				htmlCreator;

	@Resource
	private UserRepository			userRepository;

	@Resource
	private ChatCoreRepository		chatCoreRepository;

	@Resource
	private ChatDialogRepository	chatDialogRepository;

	/**
	 * Main url for admin panel.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		String username = getUserName();

		ChatDialog chatDialog = this.chatDialogRepository.findByUserName(username);

		List<ChatDialogDTO> chats = groupDialogs(chatDialog);

		model.addObject("chats", chats);
		model.addObject("chatCore", this.htmlCreator.getBaseHtml(username));
		model.setViewName("admin");
		return model;

	}

	@RequestMapping(value = "/saveChatCore", method = RequestMethod.POST)
	public ModelAndView saveChatCore(@ModelAttribute(value = "chatCoreDto") ChatCoreDTO chatCoreDto) {

		ModelAndView model = new ModelAndView();
		User user = this.userRepository.findOne(getUserName());

		ChatCore chatCore = this.chatCoreRepository.findByUserId(user.getUsername());

		chatCore = initialiseChatCore(user, chatCore);

		chatCore.setCss(chatCoreDto.getCss());
		chatCore.setHtml(chatCoreDto.getHtml());

		this.chatCoreRepository.save(chatCore);

		model.addObject("chatCore", chatCoreDto);
		model.addObject("message", "Saved succesfully!");
		model.addObject("situation", "chatCoreForm");
		model.setViewName("admin");
		return model;
	}

	private ChatCore initialiseChatCore(User user, ChatCore chatCore) {
		if (chatCore == null) {
			chatCore = new ChatCore();
			chatCore.setUser(user);
		}
		return chatCore;
	}

	private List<ChatDialogDTO> groupDialogs(ChatDialog chatDialog) {
		List<ChatDialogDTO> chatDialogDtoList = new ArrayList<ChatDialogDTO>();
		Map<String, List<MessageDTO>> mapDialog = new HashMap<String, List<MessageDTO>>();
		if (chatDialog != null && chatDialog.getMessages() != null && !chatDialog.getMessages().isEmpty()) {
			for (Message message : chatDialog.getMessages()) {
				List<MessageDTO> messages = new ArrayList<MessageDTO>();

				if (mapDialog.containsKey(message.getUsername())) {
					messages = mapDialog.get(message.getUsername());
				}

				MessageDTO messageDTO = new MessageDTO();
				BeanUtils.copyProperties(message, messageDTO);
				messages.add(messageDTO);
				mapDialog.put(message.getUsername(), messages);
			}
		}
		for (String key : mapDialog.keySet()) {
			ChatDialogDTO chatDialogDto = new ChatDialogDTO();
			chatDialogDto.setUserFrom(key);
			chatDialogDto.setMessages(mapDialog.get(key));
			chatDialogDtoList.add(chatDialogDto);
		}
		System.out.println("size :: " + chatDialogDtoList.size());
		return chatDialogDtoList;

	}

}

package com.ckminimal.chat.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ckminimal.chat.dto.ChatCoreDTO;
import com.ckminimal.chat.entities.ChatCore;
import com.ckminimal.chat.repositories.ChatDialogRepository;
import com.ckminimal.chat.repositories.MessageRepository;
import com.ckminimal.chat.repositories.UserRepository;
import com.ckminimal.chat.service.HtmlCreator;
import com.ckminimal.chat.service.NotificationCache;

@RestController
public class WebChatController {

	@Resource
	private NotificationCache		notificationCache;

	@Resource
	private HtmlCreator				htmlCreator;

	@Resource
	private UserRepository			userRepository;

	@Resource
	private ChatDialogRepository	chatDialogRepository;

	@Resource
	private MessageRepository		messageRepository;

	@RequestMapping(value = "/chatCore/{username}", method = RequestMethod.GET)
	public ChatCoreDTO baseHtml(@PathVariable(value = "username") String username) {
		ChatCoreDTO chatCoreDto = new ChatCoreDTO();
		ChatCore chatCore = this.htmlCreator.getBaseHtml(username);
		chatCoreDto.setCss(chatCore.getCss());
		chatCoreDto.setHtml(chatCore.getHtml());
		return chatCoreDto;
	}
}

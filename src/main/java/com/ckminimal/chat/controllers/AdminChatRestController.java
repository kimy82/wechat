package com.ckminimal.chat.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ckminimal.chat.entities.ChatDialog;
import com.ckminimal.chat.entities.Message;
import com.ckminimal.chat.repositories.ChatDialogRepository;
import com.ckminimal.chat.repositories.MessageRepository;
import com.ckminimal.chat.service.NotificationCache;

/**
 * Admin rest calls
 * 
 * @author kim
 *
 */
@RestController
public class AdminChatRestController extends HelperController {

	@Resource
	private ChatDialogRepository	chatDialogRepository;

	@Resource
	private MessageRepository		messageRepository;

	@Resource
	private NotificationCache		notificationCache;

	@RequestMapping(value = "/deleteChat/{userFrom}", method = RequestMethod.DELETE)
	public void deleteChat(@PathVariable(value = "userFrom") String userFrom) {
		ChatDialog chatDialog = this.chatDialogRepository.findByUserName(this
				.getUserName());
		List<Message> messagesListToRemove = new ArrayList<Message>();
		List<Integer> indexToRemove = new ArrayList<Integer>();
		int index = 0;
		for (Message message : chatDialog.getMessages()) {
			if (message.getUsername().equals(userFrom.trim())) {
				indexToRemove.add(index);
				messagesListToRemove.add(message);
			}
			index++;
		}

		for (Message message : messagesListToRemove) {
			this.messageRepository.deleteByUserName(message.getId());
		}
	}

}

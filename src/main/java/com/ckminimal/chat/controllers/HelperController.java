package com.ckminimal.chat.controllers;

import org.springframework.security.core.context.SecurityContextHolder;

public class HelperController {

	protected String getUserName() {
		return SecurityContextHolder.getContext()
				.getAuthentication().getName();
	}
}

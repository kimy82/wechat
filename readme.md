##Chat

### What is this repository for? ###

* The project is an old and not finished project. However, It was supossed to be a one to many chat. The clients send messages to the admin and the admin talk to the clients and manage open chats. The technology used is **spring deferred results** to notify the clients and admin for new messages.

### How do I get set up? ###

* create a database in Mysql named _lifechat_ -> _create database lifechat;_
* Insert the example user _insert into users(username,email, password, enabled) values ('eee','eee@gmail.com','eee',1);_
* Insert the adminrole for the user _insert into user_roles (id, authority, user_username) values (1, 'ROLE_ADMIN','eee');_
* Import the project into STS and run it as a Spring boot project
* The application is running in _localhost:8080_
* In the external folder there is the encapsulated html and js that is supposed to be embedded in an external website. Open the lifeChatDemo.html with Chrome an send a message. You will see the chat in the admin panel if you refresh the page.
var host = "http://localhost:8080"

/**
*Loading of necessary javascripts 	 
*/
var loadingJsController = {
	_isJQuery : function() {
		if (typeof $ === 'undefined')
			return false;
		else
			return true;
	},
	loadJQuery : function() {
		if (loadingJsController._isJQuery) {
			loadingJsController.loadJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js",loadingChat.getChatCore);
		}
	},
	loadJS : function(url, callback){
		    var script = document.createElement("script")
		    script.type = "text/javascript";

		    if (script.readyState){
		        script.onreadystatechange = function(){
		            if (script.readyState == "loaded" ||
		                    script.readyState == "complete"){
		                script.onreadystatechange = null;
		                callback();
		            }
		        };
		    } else {
		        script.onload = function(){
		            callback();
		        };
		    }

		    script.src = url;
		    document.getElementsByTagName("head")[0].appendChild(script);
	}
}

/**
 * Life chat options initialise
 */
var lifeChat = {
		init : function(options){
			if(typeof options.username === 'undefined'){
				throw 'You must set a username!';
			}
			lifeChat.options = options;
			loadingChat.init();
		},
		enviaMessage : function(){
			if($('#userFrom').val() == ''){
				alert("You must enter a username");
				return;
			}
			if($('#message').val() == ''){
				alert("You must enter a message");
				return;
			}
			var data={
					'userTo' : lifeChat.options.username,
					'userFrom' : $('#userFrom').val(),
					'message':  $('#message').val(),
			}
			$.ajax({
				  type: "POST",
				  url: host+"/sentMessage",
				  data: data,
				  success: lifeChat.populateMessage,
				});
		},
		populateMessage : function(data){
			console.log(data);
		},
		checkMessages : function(){
			
			lifeChat._checkUserIsThere();
			var userFrom = $('#userFrom').val();
			if(userFrom == ''){
				setTimeout(function(){lifeChat.checkMessages();},3000);
				return;
			}
			
			$.ajax({
				  type: "GET",
				  url: host+"/checkMessages/"+userFrom,
				}).done(lifeChat.populateReceivedMessage)
				.fail(function(e){console.log(e);});
		},
		populateReceivedMessage : function(data){
			console.log(data);
			if(data.messages != null && data.messages.length > 0){
				for(i=0;i<data.messages.length;i++){
					lifeChat.addNewMessage(data.messages[i].username,data.messages[i].message);
				}
			}
			lifeChat.checkMessages();
		},
		addNewMessage: function(userFrom,message){
			try{
				var messagesDiv = $('.messageslist')[0];
				var div =  document.createElement("div");
				$(div).html(message);
				$(messagesDiv).append(div);
				lifeChat.scrollBottom(messagesDiv);
			}catch(error){}
		},
		_checkUserIsThere : function(){
			if(typeof lifeChat.options.username === 'undefined'){
				throw 'user is undefined';
			}
		},
		scrollBottom : function(div){
			try{
				var height = div.scrollHeight;
				$(div).scrollTop(height);
			}catch(error){
				console.log("It is not scrollable:: "+messagesDiv)
			}
			
		}
}

/**
 * Builds the chat
 */
var loadingChat = {
	init : function() {
		loadingJsController.loadJQuery();
	},
	getChatCore : function() {
		
		$.get(host+"/chatCore/"+lifeChat.options.username, function(data) {
			loadingChat.pasteChatInPage(data)
		}).fail(function() {
			throw "No connection with the host: "+host;
		});
	},
	pasteChatInPage : function(data){
		$('head').append('<style>'+ data.css +'</style>');
		$( 'body' ).append(data.html);
		$('#enviaMessage').on('click', lifeChat.enviaMessage);
		//start checking messages.
		lifeChat.checkMessages();
	}
}

	
